﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using AkBarsInternship.Models;
using System.Data.Entity;

namespace AkBarsInternship.Controllers
{
    public class RegistrationController : ApiController
    {
        string defaultAnswer = "send here your login and password in JSON. Example:{ \"Phone\": \"8 - 800 - 555 - 35 - 35\",\"Password\": \"666\"}";

        //GET api/registration
        [HttpGet]
        public string Default()
        {
            return defaultAnswer;
        }

        //POST api/registration
        [HttpPost]
        public string Register([FromBody]LoginPassword login)
        {
            if (login == null)
                return defaultAnswer;

            bool check = checkWithSMS();
            if (check)
            {
                LoginPasswordContext db = new LoginPasswordContext();
                db.LoginPasswords.Add(login);
                db.SaveChanges();
                return "Registration successful";
            }
            else
            {
                return "SMS check failed";
            }
        }

        private bool checkWithSMS()
        {
            return true;
        }
    }
}
