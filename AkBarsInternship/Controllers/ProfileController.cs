﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using AkBarsInternship.Models;
using System.Data.Entity;

namespace AkBarsInternship.Controllers
{
    public class ProfileController : ApiController
    {

        //GET: api/profile
        [HttpGet]
        public string Default()
        {
            return "Token required: api/profile/{token}";
        }

        //GET: api/profile/{token}
        [HttpGet]
        public Profile Details(string token)
        {
            //string token = "8-800-555-35-35|666";
            //У нас запросили какой то логин. Теперь нужно по токену получить подходящий профиль и вернуть его.
            string login = loginFromToken(token);

            ProfileContext db = new ProfileContext();
            return db.Profiles.Find(login);
        }
        
        //POST: api/profile/{token}
        [HttpPost]
        public string Edit(string token, Profile profile)
        {
            string login = loginFromToken(token);

            try
            {
                ProfileContext db = new ProfileContext();

                db.Entry(profile).State = EntityState.Modified;
                //db.e
                db.SaveChanges();
                return "Profile updated";
            }
            catch(Exception e)
            {
                return "Update failed: " + e.Message;
            }
        }

        // GET: api/profile/{token}
        public string Delete(string token)
        {
            //ToDo: Удаление зарегистрированного профиля и его пары логин/пароль
            return "None";
        }

        private string loginFromToken(string token)
        {
            return token.Split('|')[0];
        }
    }
}
