﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AkBarsInternship.Models;
using System.Data.Entity;


namespace AkBarsInternship.Controllers
{
    public class LoginController : ApiController
    {
        LoginPasswordContext db = new LoginPasswordContext();

        //GET api/login
        [HttpGet]
        public IEnumerable<LoginPassword> GetLogins() //Функция только для теста
        {
            //Лучше возвращать только логины, без паролей. Пусть это и тестовая функция.
            return db.LoginPasswords;
        }

        //POST api/login
        [HttpPost]
        public string GetAccess([FromBody]LoginPassword login)
        {
            if (login == null)
                return @"send here your login and password in JSON or register through api/registration. Example:{ ""Phone"": ""8-800-555-35-35"",""Password"": ""666""}";
            
            //Поиск в бд этого логина и сравнение пароля
            LoginPassword lg = db.LoginPasswords.Find(login.Phone);
            if (lg.Password == login.Password)
            {
                //Токен нужно шифровать. Плюс желательно добавить время окончания действия. Здесь как есть, для наглядности.
                string token = login.Phone + "|" + login.Password;
                return token;
            }
            else
            {
                return "Incorrect password";
            }
        }
    }
}