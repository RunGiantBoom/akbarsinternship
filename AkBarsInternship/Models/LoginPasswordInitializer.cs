﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AkBarsInternship.Models
{
    public class LoginPasswordInitializer : DropCreateDatabaseAlways<LoginPasswordContext>
    {
        protected override void Seed(LoginPasswordContext context)
        {
            context.LoginPasswords.Add(new LoginPassword { Phone = "8-800-555-35-35", Password = "666" });
            base.Seed(context);
        }
    }
}