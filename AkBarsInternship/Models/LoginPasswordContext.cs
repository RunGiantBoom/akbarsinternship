﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AkBarsInternship.Models
{
    public class LoginPasswordContext : DbContext //DbContext реализует паттерн репозиторий
    {
        public DbSet<LoginPassword> LoginPasswords { get; set; }
    }
}