﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AkBarsInternship.Models
{
	public class ProfileInitializer : DropCreateDatabaseAlways<ProfileContext>
    {
        protected override void Seed(ProfileContext context)
        {
            context.Profiles.Add(new Profile { Phone = "8-800-555-35-35", Name = "Не", Surname = "Отвечать" });
            base.Seed(context);
        }
    }
}