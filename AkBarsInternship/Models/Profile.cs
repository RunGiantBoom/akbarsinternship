﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AkBarsInternship.Models
{
    public class Profile
    {
        [Key]
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string ImageURL { get; set; }
    }
}